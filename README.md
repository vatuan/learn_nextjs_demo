# Chạy :

- yarn dev

# Cài đặt Typescript với NextJs

- tại folder root , chạy : `tsc --init` ==> tự động tạo ra file `tsconfig.json`, hoặc có thể tự tạo bằng cách nhập tên file `tsconfig.json`
- chạy tiếp : `yarn add --dev typescript @types/react @types/node` nếu sử dụng yarn
- hoặc : `npm --save-dev typescript @types/react @types/node` nếu sử dụng npm
- đổi các file có đuôi là `.js` sang `.tsx`

# Route

- những file nào nằm trong folder `pages` hoặc `src/pages` thì route sẽ là tên file đó
- file có tên là `index.js` thì khi chạy, mặc định sẽ được chạy đầu tiên

![alt](src/assets/images/Route01.png)

Với ví dụ trên thì khi chạy projct, với đường dẫn là `http://localhost:3000` thì sẽ chạy vào trang `index.js`, và với đường dẫn là `http://localhost:3000/details` thì sẽ chạy vào trang `details.js`

#### Nếu trong folder pages có folder khác

![alt](src/assets/images/Route02.png)

- Ở trong ví dụ này ta thấy trong folder `**pages**` có thêm 1 folder khác có tên là `**vehicle**`
- Như vậy `vehicle` sẽ đóng vai trò là 1 route,
- với địa chỉ `http://localhost:3000/vehicle` thì sẽ chạy vào trang `vehicle/index.js`
- với địa chỉ `http://localhost:3000/vehicle/bruno` thì sẽ chạy vào trang `vehicle/bruno.js`

# NextJs không cần import React

- NextJs **Không cần** `import React from 'react'`
